import { createStackNavigator, createAppContainer } from 'react-navigation';
import instructions from './src/pages/instructions';
import starts from './src/pages/start';
import setting from './src/pages/setting';
import home from './src/pages/home';
import congra from './src/pages/congratulations';
import { Button } from 'react-native';
import React, { Component } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';

const resetAction = StackActions.reset({
	index: 0,
	key: null,
	actions: [ NavigationActions.navigate({ routeName: 'home' }) ]
});
class App extends React.Component {
	componentDidMount = () => {
		setTimeout(() => {
			this.props.navigation.dispatch(resetAction);
		}, 1000);
	};
	render() {
		return (
			<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
				<Text>Loading......</Text>
				<ActivityIndicator size="small" color="#00ff00" />
			</View>
		);
	}
}
const HomestacK = createStackNavigator({
	home: {
		screen: home,
		navigationOptions: {
			header: null
		}
	},
	ins: {
		screen: instructions,
		navigationOptions: {
			title: 'Instruction',
			headerStyle: {
				backgroundColor: 'rgb(246, 145, 3)'
			},
			headerTitleStyle: {
				fontWeight: 'bold',
				fontSize: 25,
				color: 'white'
			},
			headerTintColor: 'white'
		}
	},
	starts: {
		screen: starts,
		navigationOptions: {
			title: '7 MINUTE',
			headerStyle: {
				backgroundColor: 'rgb(246, 145, 3)'
			},
			headerTitleStyle: {
				fontWeight: 'bold',
				fontSize: 25,
				color: 'white'
			},
			headerTintColor: 'white'
		}
	},
	setting: {
		screen: setting,
		navigationOptions: {
			title: 'Setting',
			headerStyle: {
				backgroundColor: 'rgb(246, 145, 3)'
			},
			headerTitleStyle: {
				fontWeight: 'bold',
				fontSize: 25,
				color: 'white'
			},
			headerTintColor: 'white'
		}
	},
	congra: {
		screen: congra,
		navigationOptions: {
			header: null
		}
	}
});

const AppNavigator = createStackNavigator({
	App: {
		screen: App,
		navigationOptions: {
			header: null
		}
	},
	home: {
		screen: HomestacK,
		navigationOptions: {
			header: null
		}
	}
});

export default createAppContainer(AppNavigator);
