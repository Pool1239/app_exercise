import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, AsyncStorage } from 'react-native';
// import { CircularProgress } from "react-native-svg-circular-progress"
import { CircularProgress } from '../lip/svg1/index';
import { CircularProgress2 } from '../lip/svg2/index';
import { CircularProgress3 } from '../lip/svg3/index';
import styles from '../css/Css_start';
import datas from '../data/data_start';

export default class start extends Component {
	constructor(props) {
		super(props);

		this.state = {
			percentage: 15,
			stop: false,
			step: 1,
			round: 0,
			rest: '',
			set: '',
			fixrest: '',
			fixset: '',
			showdata: [],
			showdata2: []
		};
	}
	componentDidMount = async () => {
		this._subscribe = this.props.navigation.addListener('didFocus', () => {
			this.setState({
				percentage: 15,
				stop: false,
				step: 1,
				round: 0,
				rest: '',
				set: '',
				fixrest: '',
				fixset: '',
				showdata: datas.data,
				showdata2: datas.data2
			});
			this._retrieveData();
		});
	};

	_retrieveData = async () => {
		try {
			const value = await AsyncStorage.getItem('Exercise');
			if (value !== null) {
				let settingJson = JSON.parse(value);
				// alert(settingJson.rest)
				this.setState({
					rest: settingJson.rest,
					set: settingJson.set,
					fixrest: settingJson.rest,
					fixset: settingJson.set
				});
			}
		} catch (error) {
			alert(error);
			// Error retrieving data
		}
	};

	step1 = (check) => {
		if (check === 1) {
			// กด play และ รอหมดเวลา และ Back มาจาก step2
			let { stop, percentage } = this.state;
			if (!stop && percentage) {
				this.setState({ stop: true });
				this.timer = setInterval(() => {
					const newCount = this.state.percentage - 1;
					if (newCount > 0) {
						this.setState({ percentage: newCount });
					} else {
						clearInterval(this.timer);
						this.setState((prev) => ({
							percentage: prev.percentage * 0 + this.state.set,
							round: prev.round + 1,
							step: prev.step + 1,
							stop: false
						}));
						this.step2(1);
					}
				}, 1000);
			}
		} else if (check === 2) {
			// กด Next ไป step2
			let { percentage } = this.state;
			if (percentage > 0 && this.timer) {
				clearInterval(this.timer);
				this.setState(
					(prev) => ({
						percentage: prev.percentage * 0 + this.state.set,
						step: prev.step + 1,
						round: prev.round + 1,
						stop: false
					}),
					() => {
						if (this.state.stop === false) {
							this.step2(1);
						}
					}
				);
			} else {
				this.setState(
					(prev) => ({
						percentage: prev.percentage * 0 + this.state.set,
						step: prev.step + 1,
						round: prev.round + 1,
						stop: false
					}),
					() => this.step2(1)
				);
			}
		}
	};
	step2 = (check) => {
		if (check === 1) {
			// รอจนหมดเวลามาจาก step1
			let { stop, percentage } = this.state;
			if (!stop && percentage) {
				this.setState({ stop: true });
				this.timer = setInterval(() => {
					const newCount = this.state.percentage - 1;
					if (newCount > 0) {
						this.setState({ percentage: newCount });
					} else {
						let { round } = this.state;
						clearInterval(this.timer);
						if (round === 21) {
							this.setState({ percentage: 0 });
							this.props.navigation.navigate('congra');
						} else {
							this.setState((prev) => ({
								percentage: prev.percentage * 0 + this.state.rest,
								round: prev.round + 1,
								step: prev.step + 1,
								stop: false
							}));
							this.step3(1);
						}
					}
				}, 1000);
			}
		} else if (check === 2) {
			// กด Next ไป step3
			let { percentage } = this.state;
			if (percentage > 0 && this.timer) {
				clearInterval(this.timer);
				this.setState(
					(prev) => ({
						percentage: prev.percentage * 0 + this.state.rest,
						step: prev.step + 1,
						round: prev.round + 1,
						stop: false
					}),
					() => {
						if (this.state.stop === false) {
							this.step3(1);
						}
					}
				);
			} else {
				this.setState(
					(prev) => ({
						percentage: prev.percentage * 0 + this.state.rest,
						step: prev.step + 1,
						round: prev.round + 1,
						stop: false
					}),
					() => this.step3(1)
				);
			}
		} else if (check === 3) {
			// กด Back ไป step1
			let { percentage } = this.state;
			if (percentage > 0 && this.timer) {
				clearInterval(this.timer);
				this.setState(
					(prev) => ({
						percentage: prev.percentage * 0 + 15,
						step: prev.step - 1,
						round: prev.round - 1,
						stop: false
					}),
					() => {
						if (this.state.stop === false) {
							this.step1(1);
						}
					}
				);
			} else {
				this.setState(
					(prev) => ({
						percentage: prev.percentage * 0 + 15,
						step: prev.step - 1,
						round: prev.round - 1,
						stop: false
					}),
					() => {
						if (this.state.stop === false) {
							this.step1(1);
						}
					}
				);
			}
		} else if (check === 4) {
			// กด Back ไป step3
			let { percentage } = this.state;
			if (percentage > 0 && this.timer) {
				clearInterval(this.timer);
				this.setState(
					(prev) => ({
						percentage: prev.percentage * 0 + this.state.rest,
						step: prev.step + 1,
						round: prev.round - 1,
						stop: false
					}),
					() => {
						if (this.state.stop === false) {
							this.step3(1);
						}
					}
				);
			} else {
				this.setState(
					(prev) => ({
						percentage: prev.percentage * 0 + this.state.rest,
						step: prev.step + 1,
						round: prev.round - 1,
						stop: false
					}),
					() => {
						if (this.state.stop === false) {
							this.step3(1);
						}
					}
				);
			}
		}
	};
	step3 = (check) => {
		if (check === 1) {
			//รอจนหมดเวลามาจาก step2
			let { stop, percentage } = this.state;
			if (!stop && percentage) {
				this.setState({ stop: true });
				this.timer = setInterval(() => {
					const newCount = this.state.percentage - 1;
					if (newCount > 0) {
						this.setState({ percentage: newCount });
					} else {
						clearInterval(this.timer);
						this.setState((prev) => ({
							percentage: prev.percentage * 0 + this.state.set,
							round: prev.round + 1,
							step: prev.step - 1,
							stop: false
						}));
						this.step2(1);
					}
				}, 1000);
			}
		} else if (check === 2) {
			// กด Next ไป step2
			let { percentage } = this.state;
			if (percentage > 0 && this.timer) {
				clearInterval(this.timer);
				this.setState(
					(prev) => ({
						percentage: prev.percentage * 0 + this.state.set,
						step: prev.step - 1,
						round: prev.round + 1,
						stop: false
					}),
					() => {
						if (this.state.stop === false) {
							this.step2(1);
						}
					}
				);
			} else {
				this.setState(
					(prev) => ({
						percentage: prev.percentage * 0 + this.state.set,
						step: prev.step - 1,
						round: prev.round + 1,
						stop: false
					}),
					() => this.step2(1)
				);
			}
		} else if (check === 3) {
			// กด Back ไป step2
			// alert('asdasda');
			let { percentage } = this.state;
			if (percentage > 0 && this.timer) {
				clearInterval(this.timer);
				this.setState(
					(prev) => ({
						percentage: prev.percentage * 0 + this.state.set,
						step: prev.step - 1,
						round: prev.round - 1,
						stop: false
					}),
					() => {
						if (this.state.stop === false) {
							this.step2(1);
						}
					}
				);
			} else {
				this.setState(
					(prev) => ({
						percentage: prev.percentage * 0 + this.state.set,
						step: prev.step - 1,
						round: prev.round - 1,
						stop: false
					}),
					() => {
						if (this.state.stop === false) {
							this.step2(1);
						}
					}
				);
			}
		}
	};
	_handleStop() {
		if (this.timer) {
			clearInterval(this.timer);
			this.setState({ stop: false });
		}
	}
	render() {
		return (
			<View style={styles.inspos}>
				<View style={styles.insbody}>
					<View style={styles.heights}>
						{this.state.step === 1 ? (
							<View style={styles.view_cir}>
								<Text style={styles.text_round}>Ready to go!</Text>
								<Text style={styles.text_round_1}>ท่าเตรียม"</Text>
								<Text style={styles.text_round_1}>
									ท่าที่{' '}
									{this.state.showdata2.length > 0 ? (
										this.state.showdata2[this.state.round].number
									) : null}/11
								</Text>
								<View style={styles.view_step_1}>
									<View style={{ width: '25%' }} />
									<View style={styles.view_step_2}>
										{this.state.stop === true ? (
											<TouchableOpacity onPress={() => this._handleStop()}>
												<CircularProgress percentage={this.state.percentage}>
													<View style={styles.view_cir}>
														<Text style={{ fontSize: 40, marginBottom: 5 }}>
															{this.state.percentage}
														</Text>
														<Image source={require('../img/pause.png')} />
													</View>
												</CircularProgress>
											</TouchableOpacity>
										) : (
											<TouchableOpacity
												onPress={() => {
													this.step1(1);
												}}
											>
												<CircularProgress percentage={this.state.percentage}>
													<View style={styles.view_cir}>
														<Text style={{ fontSize: 40, marginBottom: 5 }}>
															{this.state.percentage}
														</Text>
														<Image source={require('../img/play.png')} />
													</View>
												</CircularProgress>
											</TouchableOpacity>
										)}
									</View>
									<View style={styles.view_next}>
										<TouchableOpacity
											onPress={() => {
												this.step1(2);
											}}
										>
											<Image source={require('../img/next.png')} />
										</TouchableOpacity>
									</View>
								</View>
							</View>
						) : this.state.step === 2 ? (
							<View style={styles.view_cir}>
								<Text style={styles.text_round}>Round 1/1!</Text>
								<Text style={styles.text_round_1}>เริ่ม"</Text>
								<Text style={styles.text_round_1}>
									ท่าที่{' '}
									{this.state.showdata2.length > 0 ? (
										this.state.showdata2[this.state.round].number
									) : null}/11
								</Text>
								<View style={styles.view_step_1}>
									<View style={styles.view_back}>
										{this.state.round === 1 ? (
											<TouchableOpacity
												onPress={() => {
													this.step2(3);
												}}
											>
												<Image source={require('../img/back.png')} />
											</TouchableOpacity>
										) : (
											<TouchableOpacity
												onPress={() => {
													this.step2(4);
												}}
											>
												<Image source={require('../img/back.png')} />
											</TouchableOpacity>
										)}
									</View>
									<View style={styles.view_step_2}>
										{this.state.stop === true ? (
											<TouchableOpacity onPress={() => this._handleStop()}>
												<CircularProgress2
													percentage={this.state.percentage}
													fix={this.state.fixset}
												>
													<View style={styles.view_cir}>
														<Text style={{ fontSize: 40, marginBottom: 5 }}>
															{this.state.percentage}
														</Text>
														<Image source={require('../img/pause.png')} />
													</View>
												</CircularProgress2>
											</TouchableOpacity>
										) : (
											<TouchableOpacity
												onPress={() => {
													this.step2(1);
												}}
											>
												<CircularProgress2
													percentage={this.state.percentage}
													fix={this.state.fixset}
												>
													<View style={styles.view_cir}>
														<Text style={{ fontSize: 40, marginBottom: 5 }}>
															{this.state.percentage}
														</Text>
														<Image source={require('../img/play.png')} />
													</View>
												</CircularProgress2>
											</TouchableOpacity>
										)}
									</View>
									{this.state.round === 21 ? (
										<View style={{ width: '25%' }} />
									) : (
										<View style={styles.view_next}>
											<TouchableOpacity
												onPress={() => {
													this.step2(2);
												}}
											>
												<Image source={require('../img/next.png')} />
											</TouchableOpacity>
										</View>
									)}
								</View>
							</View>
						) : this.state.step === 3 ? (
							<View style={styles.view_cir}>
								<Text style={styles.text_round}>Ready!</Text>
								<Text style={styles.text_round_1}>Go!</Text>
								<Text style={styles.text_round_1}>
									ท่าเตรียม" ท่าที่{' '}
									{this.state.showdata2.length > 0 ? (
										this.state.showdata2[this.state.round].number
									) : null}/11
								</Text>
								<View style={styles.view_step_1}>
									<View style={styles.view_back}>
										<TouchableOpacity
											onPress={() => {
												this._handleStop(), this.step3(3);
											}}
										>
											<Image source={require('../img/back.png')} />
										</TouchableOpacity>
									</View>
									<View style={styles.view_step_2}>
										{this.state.stop === true ? (
											<TouchableOpacity onPress={() => this._handleStop()}>
												<CircularProgress3
													percentage={this.state.percentage}
													fix={this.state.fixrest}
												>
													<View style={styles.view_cir}>
														<Text style={{ fontSize: 40, marginBottom: 5 }}>
															{this.state.percentage}
														</Text>
														<Image source={require('../img/pause.png')} />
													</View>
												</CircularProgress3>
											</TouchableOpacity>
										) : (
											<TouchableOpacity onPress={() => this.step3(1)}>
												<CircularProgress3
													percentage={this.state.percentage}
													fix={this.state.fixrest}
												>
													<View style={styles.view_cir}>
														<Text style={{ fontSize: 40, marginBottom: 5 }}>
															{this.state.percentage}
														</Text>
														<Image source={require('../img/play.png')} />
													</View>
												</CircularProgress3>
											</TouchableOpacity>
										)}
									</View>
									<View style={styles.view_next}>
										<TouchableOpacity
											onPress={() => {
												this.step3(2);
											}}
										>
											<Image source={require('../img/next.png')} />
										</TouchableOpacity>
									</View>
								</View>
							</View>
						) : null}
					</View>
					<View style={styles.heights}>
						{this.state.showdata.length > 0 ? (
							<View style={styles.view_showdata}>
								<Image source={this.state.showdata[this.state.round].set} />
							</View>
						) : null}
					</View>
				</View>
			</View>
		);
	}
}
