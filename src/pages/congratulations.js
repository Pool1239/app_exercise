import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, BackHandler } from 'react-native';
import styles from '../css/Css_congra';
import { StackActions, NavigationActions } from 'react-navigation';
const resetAction = StackActions.reset({
	index: 0,
	key: null,
	actions: [ NavigationActions.navigate({ routeName: 'home' }) ]
});
export default class congratulations extends Component {
	componentDidMount() {
		BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
	}

	handleBackPress = () => {
		this.props.navigation.dispatch(resetAction);
		return true;
	};
	render() {
		return (
			<View style={styles.inbody}>
				<Image source={require('../img/award.png')} style={{ resizeMode: 'contain' }} />
				<Text style={styles.congtext}>Congratulations.!</Text>
				<View style={styles.inbody_3}>
					<View style={styles.inbody_3_1}>
						<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
							<Text style={styles.dura}>Doing again!</Text>
						</TouchableOpacity>
					</View>
					<View style={styles.inbody_3_2}>
						<TouchableOpacity>
							<Text style={styles.dura}>SHARE!</Text>
						</TouchableOpacity>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('home')}>
							<Text style={styles.dura}>NEXT!</Text>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		);
	}
}
