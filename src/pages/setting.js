import React, { Component } from 'react';
import {
	View,
	Text,
	StyleSheet,
	Image,
	TouchableOpacity,
	Dimensions,
	Platform,
	AsyncStorage,
	Button
} from 'react-native';
import { Content, List, ListItem } from 'native-base';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Modal from 'react-native-modal';
import CounterSetting from '../component/CounterSetting.js';
import { throwStatement } from '@babel/types';

export default class setting extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isDateTimePickerVisible: false,
			modalVisible: false,
			restModalVisible: false,
			rest: 10,
			set: 30
		};
	}

	componentDidMount() {
		this._retrieveData();
	}

	_retrieveData = async () => {
		try {
			const value = await AsyncStorage.getItem('Exercise');
			// alert(value);
			if (value !== null) {
				let settingJson = JSON.parse(value);
				// alert(settingJson.rest)
				this.setState({
					rest: settingJson.rest,
					set: settingJson.set
				});
			}
		} catch (error) {
			alert(error);
			// Error retrieving data
		}
	};

	_storeData = async () => {
		try {
			const value = await AsyncStorage.getItem('Exercise');
			if (value != null)
				await AsyncStorage.setItem(
					'Exercise',
					JSON.stringify({ rest: this.state.rest, set: this.state.set, time: '19:00' })
				);
		} catch (error) {
			alert(error);
			// Error saving data
		}
	};

	closeModal = () => {
		this.setState({ modalVisible: false, restModalVisible: false });
	};

	openModalSet = () => {
		this.setState({ modalVisible: true });
	};

	openModalRest = () => {
		this.setState({ restModalVisible: true });
	};

	onsetTime = (time) => {
		this.setState({ modalVisible: false, set: time });
		this._storeData();
	};

	onsetRestTime = (time) => {
		this.setState({ restModalVisible: false, rest: time });
		this._storeData();
	};

	_showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

	_hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

	_handleDatePicked = (date) => {
		console.log('A date has been picked: ', date);
		this._hideDateTimePicker();
	};

	render() {
		return (
			<View style={styles.inspos}>
				<View style={styles.insbody}>
					<Text
						style={{
							color: 'rgb(247, 184, 2)',
							fontSize: 25,
							fontWeight: 'bold'
						}}
					>
						WORKOUT
					</Text>
					<Content>
						<View>
							<Modal isVisible={this.state.modalVisible}>
								<CounterSetting
									close={this.closeModal}
									text="เวลาในแต่ละรอบ"
									setTime={this.onsetTime}
									time={this.state.set}
								/>
							</Modal>
							<Modal isVisible={this.state.restModalVisible}>
								<CounterSetting
									close={this.closeModal}
									text="ระยะเวลาพัก"
									setTime={this.onsetRestTime}
									time={this.state.rest}
								/>
							</Modal>
						</View>
						<List>
							<ListItem itemDivider>
								<Text>ออกกำลังกาย</Text>
							</ListItem>
							<ListItem onPress={this.openModalSet}>
								<Text>เวลาในแต่ละรอบ</Text>
							</ListItem>
							<ListItem onPress={this.openModalRest}>
								<Text>ระยะเวลาพัก</Text>
							</ListItem>
							<ListItem itemDivider>
								<Text>ทั่วไป</Text>
							</ListItem>
							<ListItem onPress={this._showDateTimePicker}>
								<Text>เวลาแจ้งเตือน</Text>
							</ListItem>
						</List>
					</Content>
					<DateTimePicker
						mode="time"
						isVisible={this.state.isDateTimePickerVisible}
						onConfirm={this._handleDatePicked}
						onCancel={this._hideDateTimePicker}
					/>
				</View>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	inspos: {
		height: '100%',
		width: '100%'
	},
	insbody: {
		backgroundColor: 'rgb(247, 246, 217)',
		width: '100%',
		height: '100%',
		paddingHorizontal: 20,
		paddingVertical: 20
	}
});
