import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import { Card, CardItem } from 'native-base';
import CalendarPicker from 'react-native-calendar-picker';
import styles from '../css/Css_home';

export default class home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedStartDate: null,
			homes: 1
		};
		this.onDateChange = this.onDateChange.bind(this);
	}

	componentDidMount = async () => {
		this._storeData();
	};

	onDateChange(date) {
		this.setState({
			selectedStartDate: date
		});
	}
	_storeData = async () => {
		try {
			const value = await AsyncStorage.getItem('Exercise');
			if (value == null)
				await AsyncStorage.setItem('Exercise', JSON.stringify({ rest: 10, set: 30, time: '19:00' }));
		} catch (error) {
			alert(error);
			// Error saving data
		}
	};

	render() {
		const { selectedStartDate } = this.state;
		const startDate = selectedStartDate ? selectedStartDate.toString() : '';
		return (
			<View style={styles.position}>
				<View style={styles.position1}>
					<View style={styles.position1_1}>
						<View style={{ marginTop: 40 }}>
							<Text style={styles.h_7min}>7 MIMUTE</Text>
						</View>
						<View style={styles.h_menusetting}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('setting')}>
								<Image source={require('../img/menu.png')} />
							</TouchableOpacity>
						</View>
					</View>
					<View style={styles.position1_2}>
						<View style={styles.h_workout_1}>
							<TouchableOpacity
								onPress={() => {
									this.setState({ homes: 1 });
								}}
							>
								<View style={styles.h_workout_2}>
									<Text style={styles.h_workout_3}>WORKOUT</Text>
								</View>
								<View style={this.state.homes === 1 ? styles.lineleft : styles.lineright} />
							</TouchableOpacity>
						</View>
						<View style={styles.h_calendar_1}>
							<TouchableOpacity
								onPress={() => {
									this.setState({ homes: 0 });
								}}
							>
								<View style={styles.h_calendar_2}>
									<Text style={styles.h_calendar_3}>CALENDAR</Text>
								</View>
								<View style={this.state.homes === 1 ? styles.lineright : styles.lineleft} />
							</TouchableOpacity>
						</View>
					</View>
				</View>
				{this.state.homes === 1 ? (
					<View style={styles.position2}>
						<Card style={styles.position2_1}>
							<View style={styles.classic}>
								<Image source={require('../img/test1.jpg')} style={styles.imgtest1} />
								<View style={styles.classic_1}>
									<Text style={styles.text_classic}>Classic</Text>
								</View>
							</View>
							<View style={styles.details}>
								<Text>
									Scientifically proven to assist weight loss and improve cardiovascular function
								</Text>
							</View>
							<View style={styles.ins}>
								<TouchableOpacity onPress={() => this.props.navigation.navigate('ins')}>
									<Text style={styles.ins_text}>INSTRUCTIONS</Text>
								</TouchableOpacity>
								<TouchableOpacity onPress={() => this.props.navigation.navigate('starts')}>
									<Text style={styles.text_start}>START</Text>
								</TouchableOpacity>
							</View>
						</Card>
					</View>
				) : (
					<View style={styles.position2}>
						<Card style={styles.cards}>
							<CalendarPicker onDateChange={this.onDateChange} />
						</Card>
						<Card style={styles.cards_2}>
							<View style={styles.cards_view}>
								<Text style={styles.total}>0</Text>
								<Text>Total for the last 30 days</Text>
							</View>
						</Card>
					</View>
				)}
			</View>
		);
	}
}
