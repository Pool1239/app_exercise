import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import datas from '../data/data_instruction';
import styles from '../css/Css_instruction';

export default class instructions extends Component {
	render() {
		return (
			<View style={styles.inspos}>
				<KeyboardAwareScrollView>
					<View style={styles.insbody}>
						<Text style={styles.t_classic}>Classic</Text>
						{datas.data.map((el) => {
							return (
								<View>
									<Text style={styles.move}>ท่าที่ {el.id}/11</Text>
									<View style={styles.detail}>
										<View style={styles.views}>
											<Image source={el.imgfirst} />
											<Text style={{ width: '100%', textAlign: 'center' }}>{el.detailfirst}</Text>
										</View>
										<View style={styles.views2}>
											<Image source={el.imgcecond} />
											<Text style={{ width: '100%', textAlign: 'center' }}>
												{el.detailsecond}
											</Text>
										</View>
									</View>
								</View>
							);
						})}
					</View>
				</KeyboardAwareScrollView>
			</View>
		);
	}
}
