import React, {Component} from "react";
import { View, StyleSheet,TouchableOpacity,Image, Text } from "react-native";

export default class CounterSetting extends Component {
constructor(props){
    super(props)
    const { time,text } = this.props
    this.state = {
        counter : time,
        text : text
    }
  }

  onConfirmClick = () => {
    // const order = ...
    this.props.setTime(this.state.counter)
  }

  onCancelClick= () =>{
    this.props.close()
  }

  _onNext=()=>{
    const { counter } = this.state
        if (counter > 10 && counter < 60){
            this.setState({
                counter : counter +1
            })
        }else if (counter == 10){
            this.setState({
                counter : counter +1
            })
        }
  }

  _onPrevios=()=>{
    const { counter } = this.state
    if (counter > 10 && counter <= 60){
        this.setState({
            counter : counter -1
        })
    }

  }

  render(){
      return(
      <View style={{backgroundColor:'white'}}>
            <Text style={{padding : 8, margin : 10, fontSize: 16, fontWeight: 'bold'}}>ตั้งค่าจำนวนวินาที {this.state.text}</Text>
            <View style={{flexDirection: 'row', justifyContent: 'space-between',  maginTop:10, paddingRight:16, paddingLeft: 16}}>
                <TouchableOpacity onPress={this._onPrevios}>
                    <Image
                        // style={styles.button}
                        source={require('../img/previous.png')}
                    />
                </TouchableOpacity>
                <Text style={{fontSize: 18}}>{this.state.counter}</Text>
                <TouchableOpacity onPress={this._onNext}>
                    <Image
                        // style={styles.button}
                        source={require('../img/next_arrow.png')}
                    />
                </TouchableOpacity>
            </View>
            <View style={{flexDirection: 'row',justifyContent: 'flex-end'}}>
                <TouchableOpacity style={{}} onPress={this.onCancelClick}>
                        <Text style={{textAlign: 'right', fontSize: 16,  color: "rgb(247, 184, 2)",padding: 16}}>ยกเลิก</Text>
                    </TouchableOpacity>
                <TouchableOpacity style={{}} onPress={this.onConfirmClick}>
                        <Text style={{textAlign: 'right', fontSize: 16,  color: "rgb(247, 184, 2)",padding: 16}}>ตกลง</Text>
                    </TouchableOpacity>
            </View>
          
          
        
      </View>
      )
  }

}

