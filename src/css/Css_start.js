export default {
	inspos: {
		height: '100%',
		width: '100%'
	},
	insbody: {
		width: '100%',
		height: '100%',
		paddingHorizontal: 20,
		paddingVertical: 20,
		alignItems: 'center'
	},
	round_1: {
		width: '100%',
		height: '50%',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center'
	},
	view_step: { justifyContent: 'center', alignItems: 'center', marginTop: 20 },
	view_step_1: {
		width: '100%',
		justifyContent: 'space-around',
		alignItems: 'center',
		marginTop: 10,
		flexDirection: 'row'
	},
	view_step_2: {
		width: '50%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	view_cir: {
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'column'
	},
	view_next: {
		width: '25%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	view_back: {
		width: '25%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	view_showdata: {
		marginTop: 20
	},
	text_round: {
		color: 'rgb(247, 184, 2)',
		fontSize: 20
	},
	heights: {
		height: '50%',
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	text_round_1: {
		color: 'rgb(247, 184, 2)',
		fontSize: 20,
		marginTop: 10
	}
};

{
	/* <View
						style={styles.round_1}
					>
						{this.state.round === 1 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Ready to go!
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าเตรียม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 1/11
								</Text>
							</View>
						) : this.state.round === 2 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Round 1/1
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									เริ่ม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 1/11
								</Text>
							</View>
						) : this.state.round === 3 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Ready to go!
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าเตรียม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 2/11
								</Text>
							</View>
						) : this.state.round === 4 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Round 1/1
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									เริ่ม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 2/11
								</Text>
							</View>
						) : this.state.round === 5 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Ready to go!
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าเตรียม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 3/11
								</Text>
							</View>
						) : this.state.round === 6 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Round 1/1
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									เริ่ม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 3/11
								</Text>
							</View>
						) : this.state.round === 7 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Ready to go!
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าเตรียม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 4/11
								</Text>
							</View>
						) : this.state.round === 8 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Round 1/1
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									เริ่ม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 4/11
								</Text>
							</View>
						) : this.state.round === 9 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Ready to go!
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าเตรียม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 5/11
								</Text>
							</View>
						) : this.state.round === 10 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Round 1/1
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									เริ่ม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 5/11
								</Text>
							</View>
						) : this.state.round === 11 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Ready to go!
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าเตรียม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 6/11
								</Text>
							</View>
						) : this.state.round === 12 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Round 1/1
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									เริ่ม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 6/11
								</Text>
							</View>
						) : this.state.round === 13 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Ready to go!
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าเตรียม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 7/11
								</Text>
							</View>
						) : this.state.round === 14 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Round 1/1
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									เริ่ม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 7/11
								</Text>
							</View>
						) : this.state.round === 15 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Ready to go!
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าเตรียม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 8/11
								</Text>
							</View>
						) : this.state.round === 16 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Round 1/1
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									เริ่ม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 8/11
								</Text>
							</View>
						) : this.state.round === 17 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Ready to go!
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าเตรียม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 9/11
								</Text>
							</View>
						) : this.state.round === 18 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Round 1/1
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									เริ่ม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 9/11
								</Text>
							</View>
						) : this.state.round === 19 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Ready to go!
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าเตรียม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 10/11
								</Text>
							</View>
						) : this.state.round === 20 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Round 1/1
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									เริ่ม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 10/11
								</Text>
							</View>
						) : this.state.round === 21 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Ready to go!
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าเตรียม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 11/11
								</Text>
							</View>
						) : this.state.round === 22 ? (
							<View
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									flexDirection: 'column'
								}}
							>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20
									}}
								>
									Round 1/1
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									เริ่ม"
								</Text>
								<Text
									style={{
										color: 'rgb(247, 184, 2)',
										fontSize: 20,
										marginTop: 10
									}}
								>
									ท่าที่ 11/11
								</Text>
							</View>
						) : null}
						<View style={styles.view_step}>
							{this.state.step === 0 ? (
								<TouchableOpacity
									onPress={() => {
										this.setState({ percentage: 15, step: 1 }), this.step1();
									}}
								>
									<View
										style={{
											width: 150,
											height: 150,
											alignItems: 'center',
											borderRadius: 80,
											borderWidth: 6,
											borderColor: 'rgb(246, 125, 90)',
											backgroundColor: 'rgb(246, 145, 10)',
											justifyContent: 'center',
											marginTop: 50
										}}
									>
										<Text style={{ color: 'white', fontSize: 25, fontWeight: 'bold' }}>เริ่ม</Text>
									</View>
								</TouchableOpacity>
							) : this.state.step === 1 ? (
								<View
									style={{
										width: '100%',
										justifyContent: 'space-around',
										alignItems: 'center',
										marginTop: 10,
										flexDirection: 'row'
									}}
								>
									<View style={{ width: '25%' }} />
									<View
										style={{
											width: '50%',
											justifyContent: 'center',
											alignItems: 'center'
										}}
									>
										{this.state.stop === true ? (
											<TouchableOpacity onPress={() => this._handleStop()}>
												<CircularProgress percentage={this.state.percentage}>
													<View
														style={{
															justifyContent: 'center',
															alignItems: 'center',
															flexDirection: 'column'
														}}
													>
														<Text style={{ fontSize: 40, marginBottom: 5 }}>
															{this.state.percentage}
														</Text>
														<Image source={require('../img/pause.png')} />
													</View>
												</CircularProgress>
											</TouchableOpacity>
										) : (
											<TouchableOpacity
												onPress={() => {
													this.step1(1);
												}}
											>
												<CircularProgress percentage={this.state.percentage}>
													<View
														style={{
															justifyContent: 'center',
															alignItems: 'center',
															flexDirection: 'column'
														}}
													>
														<Text style={{ fontSize: 40, marginBottom: 5 }}>
															{this.state.percentage}
														</Text>
														<Image source={require('../img/play.png')} />
													</View>
												</CircularProgress>
											</TouchableOpacity>
										)}
									</View>
									<View
										style={{
											width: '25%',
											justifyContent: 'center',
											alignItems: 'center'
										}}
									>
										<TouchableOpacity
											onPress={() => {
												this.step1(2);
											}}
										>
											<Image source={require('../img/next.png')} />
										</TouchableOpacity>
									</View>
								</View>
							) : this.state.step === 2 ? (
								<View
									style={{
										width: '100%',
										justifyContent: 'space-around',
										alignItems: 'center',
										marginTop: 10,
										flexDirection: 'row'
									}}
								>
									<View
										style={{
											width: '25%',
											justifyContent: 'center',
											alignItems: 'center'
										}}
									>
										{this.state.round === 2 ? (
											<TouchableOpacity
												onPress={() => {
													this.step2(3);
												}}
											>
												<Image source={require('../img/back.png')} />
											</TouchableOpacity>
										) : (
											<TouchableOpacity
												onPress={() => {
													this.step2(4);
												}}
											>
												<Image source={require('../img/back.png')} />
											</TouchableOpacity>
										)}
									</View>
									<View
										style={{
											width: '50%',
											justifyContent: 'center',
											alignItems: 'center'
										}}
									>
										{this.state.stop === true ? (
											<TouchableOpacity onPress={() => this._handleStop()}>
												<CircularProgress2
													percentage={this.state.percentage}
													fix={this.state.fixset}
												>
													<View
														style={{
															justifyContent: 'center',
															alignItems: 'center',
															flexDirection: 'column'
														}}
													>
														<Text style={{ fontSize: 40, marginBottom: 5 }}>
															{this.state.percentage}
														</Text>
														<Image source={require('../img/pause.png')} />
													</View>
												</CircularProgress2>
											</TouchableOpacity>
										) : (
											<TouchableOpacity
												onPress={() => {
													this.step2(1);
												}}
											>
												<CircularProgress2
													percentage={this.state.percentage}
													fix={this.state.fixset}
												>
													<View
														style={{
															justifyContent: 'center',
															alignItems: 'center',
															flexDirection: 'column'
														}}
													>
														<Text style={{ fontSize: 40, marginBottom: 5 }}>
															{this.state.percentage}
														</Text>
														<Image source={require('../img/play.png')} />
													</View>
												</CircularProgress2>
											</TouchableOpacity>
										)}
									</View>
									{this.state.round === 22 ? (
										<View style={{ width: '25%' }} />
									) : (
										<View
											style={{
												width: '25%',
												justifyContent: 'center',
												alignItems: 'center'
											}}
										>
											<TouchableOpacity
												onPress={() => {
													this.step2(2);
												}}
											>
												<Image source={require('../img/next.png')} />
											</TouchableOpacity>
										</View>
									)}
								</View>
							) : this.state.step === 3 ? (
								<View
									style={{
										width: '100%',
										justifyContent: 'space-around',
										alignItems: 'center',
										marginTop: 10,
										flexDirection: 'row'
									}}
								>
									<View
										style={{
											width: '25%',
											justifyContent: 'center',
											alignItems: 'center'
										}}
									>
										<TouchableOpacity
											onPress={() => {
												this._handleStop(), this.step3(3);
											}}
										>
											<Image source={require('../img/back.png')} />
										</TouchableOpacity>
									</View>
									<View
										style={{
											width: '50%',
											justifyContent: 'center',
											alignItems: 'center'
										}}
									>
										{this.state.stop === true ? (
											<TouchableOpacity onPress={() => this._handleStop()}>
												<CircularProgress3
													percentage={this.state.percentage}
													fix={this.state.fixrest}
												>
													<View
														style={{
															justifyContent: 'center',
															alignItems: 'center',
															flexDirection: 'column'
														}}
													>
														<Text style={{ fontSize: 40, marginBottom: 5 }}>
															{this.state.percentage}
														</Text>
														<Image source={require('../img/pause.png')} />
													</View>
												</CircularProgress3>
											</TouchableOpacity>
										) : (
											<TouchableOpacity onPress={() => this.step3(1)}>
												<CircularProgress3
													percentage={this.state.percentage}
													fix={this.state.fixrest}
												>
													<View
														style={{
															justifyContent: 'center',
															alignItems: 'center',
															flexDirection: 'column'
														}}
													>
														<Text style={{ fontSize: 40, marginBottom: 5 }}>
															{this.state.percentage}
														</Text>
														<Image source={require('../img/play.png')} />
													</View>
												</CircularProgress3>
											</TouchableOpacity>
										)}
									</View>
									<View
										style={{
											width: '25%',
											justifyContent: 'center',
											alignItems: 'center'
										}}
									>
										<TouchableOpacity
											onPress={() => {
												this.step3(2);
											}}
										>
											<Image source={require('../img/next.png')} />
										</TouchableOpacity>
									</View>
								</View>
							) : null}
						</View>
					</View>
					<View style={{ width: '100%', height: '50%', justifyContent: 'center', alignItems: 'center' }}>
						{this.state.round === 1 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance1_1.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 2 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image
									source={require('../img/stance1_1.jpg')}
									style={{ resizeMode: 'stretch', width: '45%' }}
								/>
								<Image
									source={require('../img/stance1_2.jpg')}
									style={{ resizeMode: 'stretch', width: '45%' }}
								/>
							</View>
						) : this.state.round === 3 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance2_1.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 4 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image
									source={require('../img/stance2_1.jpg')}
									style={{ resizeMode: 'stretch', width: '45%' }}
								/>
								<Image
									source={require('../img/stance2_2.jpg')}
									style={{ resizeMode: 'stretch', width: '45%' }}
								/>
							</View>
						) : this.state.round === 5 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance3_1.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 6 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image
									source={require('../img/stance3_1.jpg')}
									style={{ resizeMode: 'stretch', width: '45%' }}
								/>
								<Image
									source={require('../img/stance3_2.jpg')}
									style={{ resizeMode: 'stretch', width: '45%' }}
								/>
							</View>
						) : this.state.round === 7 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance4_1.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 8 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image
									source={require('../img/stance4_1.jpg')}
									style={{ resizeMode: 'stretch', width: '45%' }}
								/>
								<Image
									source={require('../img/stance4_2.jpg')}
									style={{ resizeMode: 'stretch', width: '45%' }}
								/>
							</View>
						) : this.state.round === 9 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance5_1.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 10 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance5_2.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 11 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance6.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 12 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance6.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 13 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance7.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 14 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance7.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 15 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance8.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 16 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance8.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 17 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance9.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 18 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance9.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 19 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance10_1.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 20 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image
									source={require('../img/stance10_1.jpg')}
									style={{ resizeMode: 'stretch', width: '45%' }}
								/>
								<Image
									source={require('../img/stance10_2.jpg')}
									style={{ resizeMode: 'stretch', width: '45%' }}
								/>
							</View>
						) : this.state.round === 21 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance11.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : this.state.round === 22 ? (
							<View
								style={{
									width: '100%',
									height: '100%',
									justifyContent: 'space-evenly',
									alignItems: 'center',
									flexDirection: 'row'
								}}
							>
								<Image source={require('../img/stance11.jpg')} style={{ resizeMode: 'stretch' }} />
							</View>
						) : null}
					</View> */
}
