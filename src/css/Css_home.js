export default {
	position: {
		height: '100%',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'rgb(247, 246, 217)'
	},
	position1: {
		height: '20%',
		width: '100%',
		backgroundColor: 'rgb(246, 145, 3)',
		borderWidth: null,
		borderRadius: null
	},
	position1_1: {
		height: '50%',
		width: '100%',
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		paddingHorizontal: 40
	},
	position1_2: {
		height: '50%',
		width: '100%',
		justifyContent: 'space-around',
		alignItems: 'center',
		flexDirection: 'row',
		backgroundColor: 'rgb(246, 145, 3)'
	},
	position2: {
		height: '80%',
		width: '100%',
		alignItems: 'center',
		justifyContent: 'center'
	},
	position2_1: {
		height: '60%',
		width: '80%',
		alignItems: 'center',
		justifyContent: 'center'
	},
	classic: {
		width: '100%',
		height: '60%'
	},
	h_7min: {
		fontSize: 25,
		fontWeight: 'bold',
		color: 'white'
	},
	h_menusetting: {
		height: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 40
	},
	h_workout_1: {
		width: '50%',
		height: '100%',
		flexDirection: 'column'
	},
	h_workout_2: {
		height: '95%',
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	h_workout_3: {
		fontSize: 18,
		fontWeight: '500',
		color: 'white',
		paddingTop: 15
	},
	lineleft: {
		backgroundColor: 'white',
		height: '5%',
		width: '100%',
		marginTop: 1
	},
	lineright: {
		height: '5%',
		width: '100%',
		marginTop: 1
	},
	h_calendar_1: {
		width: '50%',
		height: '100%',
		flexDirection: 'column'
	},
	h_calendar_2: {
		height: '95%',
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	h_calendar_3: {
		fontSize: 18,
		fontWeight: '500',
		color: 'white',
		paddingTop: 15
	},
	imgtest1: {
		width: '100%',
		height: '100%',
		position: 'absolute',
		resizeMode: 'stretch'
	},
	classic_1: {
		justifyContent: 'flex-end',
		paddingHorizontal: 10,
		paddingVertical: 10,
		height: '100%'
	},
	text_classic: {
		color: 'white',
		fontSize: 18,
		fontWeight: '500'
	},
	details: {
		width: '100%',
		height: '25%',
		paddingHorizontal: 10,
		paddingVertical: 10,
		justifyContent: 'center',
		alignItems: 'center'
	},
	ins: {
		width: '100%',
		height: '15%',
		alignItems: 'center',
		paddingHorizontal: 20,
		flexDirection: 'row'
	},
	ins_text: {
		color: 'rgb(247, 184, 2)',
		fontSize: 18,
		fontWeight: '500'
	},
	text_start: {
		marginLeft: 10,
		color: 'rgb(247, 184, 2)',
		fontSize: 18,
		fontWeight: '500'
	},
	cards: {
		height: '70%',
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	cards_2: {
		height: '22%',
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	cards_view: {
		height: '100%',
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'column'
	},
	total: {
		color: 'green',
		fontSize: 50,
		fontWeight: 'bold'
	}
};
