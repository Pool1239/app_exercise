export default {
	inbody: {
		height: '100%',
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#00e68a'
	},
	congtext: { color: 'white', fontWeight: '300', marginTop: 20, fontSize: 25, marginBottom: 20 },
	dura: { color: 'white', fontSize: 18 },
	inbody_3: {
		width: '100%',
		marginTop: 40,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingHorizontal: 20,
		paddingVertical: 20
	},
	inbody_3_1: {
		justifyContent: 'center',
		alignItems: 'center',
		width: '50%'
	},
	inbody_3_2: {
		justifyContent: 'space-evenly',
		alignItems: 'center',
		flexDirection: 'row',
		width: '50%'
	}
};
