export default {
	inspos: {
		height: '100%',
		width: '100%'
	},
	insbody: {
		width: '100%',
		height: '100%',
		paddingHorizontal: 20,
		paddingVertical: 20
	},
	t_classic: {
		color: 'rgb(247, 184, 2)',
		fontSize: 20,
		fontWeight: 'bold'
	},
	move: {
		marginTop: 15,
		fontSize: 17,
		fontWeight: '500'
	},
	detail: { width: '100%', flexDirection: 'column', marginTop: 10 },
	views: {
		width: '100%',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center'
	},
	views2: {
		width: '100%',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 10
	}
};
