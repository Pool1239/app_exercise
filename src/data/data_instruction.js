import stance1_1 from '../img/stance1_1.jpg';
import stance1_2 from '../img/stance1_2.jpg';
import stance2_1 from '../img/stance2_1.jpg';
import stance2_2 from '../img/stance2_2.jpg';
import stance3_1 from '../img/stance3_1.jpg';
import stance3_2 from '../img/stance3_2.jpg';
import stance4_1 from '../img/stance4_1.jpg';
import stance4_2 from '../img/stance4_2.jpg';
import stance5_1 from '../img/stance5_1.jpg';
import stance5_2 from '../img/stance5_2.jpg';
import stance6_1 from '../img/stance6_1.jpg';
import stance6_2 from '../img/stance6_2.jpg';
import stance7_1 from '../img/stance7_1.jpg';
import stance7_2 from '../img/stance7_2.jpg';
import stance8_1 from '../img/stance8_1.jpg';
import stance8_2 from '../img/stance8_2.jpg';
import stance9_1 from '../img/stance9_1.jpg';
import stance9_2 from '../img/stance9_2.jpg';
import stance10_1 from '../img/stance10_1.jpg';
import stance10_2 from '../img/stance10_2.jpg';
import stance11_1 from '../img/stance11_1.jpg';
import stance11_2 from '../img/stance11_2.jpg';
export default {
	data: [
		{
			id: 1,
			detailfirst: 'นอนราบ ชันเข่าขึ้นทั้งสองข้าง',
			detailsecond: 'ยกสะโพกขึ้นสูงสุดเท่าที่จะทำได้ นับ 1-5 แล้วเอาสะโพกลงช้าๆ',
			imgfirst: stance1_1,
			imgcecond: stance1_2
		},
		{
			id: 2,
			detailfirst: 'นอนราบ ชันเข่า ประสานมือกัน และเหยียดแขนขึ้นชี้เพดาน',
			detailsecond:
				'แกว่งแขนไปด้านซ้ายให้ไกลที่สุด พร้อมกับบิดเอวและแกว่งเข่า ทั้งสองข้างมาด้านตรงข้ามพัก และทำซ้ำด้านตรงข้าม สลับกัน ข้างละ 5 ครั้ง',
			imgfirst: stance2_1,
			imgcecond: stance2_2
		},
		{
			id: 3,
			detailfirst: 'นอนราบ ชันเข่า เหยียดแขนไปด้านหน้าและกดคางลงให้ชิดคอ',
			detailsecond: 'ยกตัวขึ้นให้ไหล่พ้นจากพื้น เอื้อมมือไปแตะหัวเข่า จากนั้น นอนราบลงพัก ทำซ้ำ 5 ครั้ง',
			imgfirst: stance3_1,
			imgcecond: stance3_2
		},
		{
			id: 4,
			detailfirst: 'นอนชันเข่า ประสานมือไว้ด้วยกัน ยกตัวขึ้นให้ไหล่พ้นพื้น เหยียดแขนไปด้านข้างของเข่า',
			detailsecond: 'แกว่งไปทางด้านขวาและด้านซ้าย สลับกันด้านละ 5 ครั้ง',
			imgfirst: stance4_1,
			imgcecond: stance4_2
		},
		{
			id: 5,
			detailfirst:
				'คุกเข่า มือยันพื้นทั้งสองข้าง เหยียดศอกตรง ก้มหัวลงระหว่างแขนทั้งสองข้าง โก่งหลังขึ้นให้สูงที่สุดเท่าที่ทำได้',
			detailsecond: 'จากนั้นให้เงยหน้าขึ้นและแอ่นหลังลงล่างให้มากที่สุด ทำสลับกัน 5 ครั้ง',
			imgfirst: stance5_1,
			imgcecond: stance5_2
		},
		{
			id: 6,
			detailfirst: 'นั่งบนเก้าอี้ สอดปลายเท้าเกี่ยวไว้กับขาเก้าอี้ทั้งสองข้าง มือซ้ายยึดพนักเก้าอี้ไว้ให้แน่น',
			imgfirst: stance6_1,
			detailsecond:
				'จากนั้นเอนตัวไปทางด้านข้างห้อยแขนขวาลงให้มือเข้าใกล้พื้นให้มากที่สุด โดยห้ามโน้มตัวมาด้านหน้า ทำสลับข้างกัน 5 ครั้ง',
			imgcecond: stance6_2
		},
		{
			id: 7,
			detailfirst:
				'นั่งเก้าอี้ สะโพกชิดพนัก สอดปลายเท้าเกี่ยวไว้กับขาเก้าอี้ทั้งสองข้าง ยกแขนขึ้นกอดอกให้อยู่ในระดับหัวไหล่',
			imgfirst: stance7_1,
			detailsecond: 'จากนั้นบิดตัวไปด้านขวาจนสุด ทำซ้ำ ด้านตรงข้าม สลับกัน 5 ครั้ง',
			imgcecond: stance7_2
		},
		{
			id: 8,
			detailfirst:
				'นั่งเก้าอี้ สะโพกชิดพนัก สอดปลายเท้า เกีียวไว้กับขาเก้าอีทั้งสองข้าง มือยึดขอบเก้าอีไว้ทั้งสองข้าง หันหน้าไปด้านซ้ายสุด โดยไม่หมุนไหล่ตาม',
			imgfirst: stance8_1,
			detailsecond: 'จากนั้นให้หันไปด้านตรงข้าม ทำสลับกัน 5 ครั้ง',
			imgcecond: stance8_2
		},
		{
			id: 9,
			detailfirst: 'ยืนหันหน้าเข้าเก้าอี้ ยกขาขวาวางส้นเท้าบนเก้าอี้ที่มีเบาะรองกันเจ็บ ขาซ้ายยืนเหยียดตรง',
			imgfirst: stance9_1,
			detailsecond:
				'จากนั้นให้โน้มตัวลงไปทางด้านหน้า พยายามให้ปลายนิ้วมือแตะปลายเท้าที่แอ่นขึ้นมา ค้างไว้นับ 1-5 พัก แล้วทำซ้ำโดยโน้มตัวลงให้ต่ำลงกว่าเดิม ทำซ้ำอีก 5 ครั้ง จากนั้นทำแบบเดียวกันในด้านตรงข้ามอีก 5 ครั้ง',
			imgcecond: stance9_2
		},
		{
			id: 10,
			detailfirst:
				'ยืนข้างเก้าอี้ มือขวาจับพนัก วางหน้าแข้งขาขวาลงบน เบาะเก้าอี้ให้ปลายเข่าอยู่บนกึ่งกลางของเบาะ',
			detailsecond:
				'ยืดลำตัวขึ้นพร้อมกับหมุนก้าวขาซ้ายมาทางด้านหน้าให้ไกลที่สุดเท่าที่ทำได้ ปล่อยพนักเก้าอี้ มือไขว้หลังเหยียดตรง งอเข่าซ้ายลดตัวให้ต่ำที่สุดโดยหลังและหน้ายืดตรง จากนั้นยืนกลับด้าน ทำในลักษณะ เดียวกันอีก 5 ครั้ง',
			imgfirst: stance10_1,
			imgcecond: stance10_2
		},
		{
			id: 11,
			detailfirst:
				'ยืนหันหลังให้กำแพง ให้หัวไหล่ สะโพก และส้นเท้าชิดกำแพงให้มากที่สุดเท่าที่จะ ทำได้เก็บคางเข้า กดหัวให้ชิดกำแพง ดึงไหล่ลง และยืดตัวขึ้นให้สูงที่สุดเท่าที่จะ ทำได้(โดยไม่ยกส้นเท้าขึ้น)',
			imgfirst: stance11_1,
			detailsecond:
				'เหยียดแขนขวาไปทางด้านหน้าและยกขึ้นจนแขนแนบใบหูและปลายหัวแม่มือชนกำแพง เอาแขนขวาลง ยกแขนซ้าย ทำแบบเดียวกัน ให้ทำ สลับข้างกัน 5 ครั้ง',
			imgcecond: stance11_2
		}
	]
};
